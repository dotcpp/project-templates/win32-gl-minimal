#ifndef APP_H
#define APP_H

#include <string>
#include <vector>

class App
{
public:
    App(const std::vector<std::string> &args);
    virtual ~App();

    bool Init();
    int Run();

    void OnInit();
    void OnFrame();
    void OnResize(int width, int height);
    void OnExit();

    template <class T>
    T *GetWindowHandle() const { return reinterpret_cast<T *>(_windowHandle); }

protected:
    const std::vector<std::string> &_args;

    template <class T>
    void SetWindowHandle(T *handle) { _windowHandle = (void *)handle; }

    void ClearWindowHandle() { _windowHandle = nullptr; }

private:
    void *_windowHandle = nullptr;
};

#endif // APP_H
